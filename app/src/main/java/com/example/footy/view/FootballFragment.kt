package com.example.footy.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.footy.model.entity.Match
import com.example.footy.ui.theme.FootyTheme
import com.example.footy.viewmodel.FootballViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate

const val TAG = "FootballFragment"

/**
 * Football fragment.
 *
 * @constructor Create empty Football fragment
 */
@AndroidEntryPoint
class FootballFragment : Fragment() {
    private val footballViewModel by viewModels<FootballViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by footballViewModel.matchListState.collectAsState()
                footballViewModel.getMatches()
                FootyTheme() {
                    // in compose world
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        if (state.isLoading) { CircularProgressIndicator() }
                        Column(modifier = Modifier.fillMaxWidth()) {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(Color(HEADER, HEADER, HEADER))
                            ) {
                                Text(
                                    text = "Spring 2019",
                                    color = Color(0, 0, TITLE_BLUE),
                                    fontSize = 25.sp,
                                    modifier = Modifier.padding(20.dp),
                                    fontWeight = FontWeight.Bold
                                )
                            }
                            LazyColumn {
                                items(state.matches) { match ->
                                    DisplayMatchInfo(match)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    companion object {
        const val TITLE_BLUE = 102
        const val HEADER = 225
    }
}

@Composable
fun DisplayMatchInfo(match: Match) {
    val dateTimeLists = match.date.split("T")
    val dateList = dateTimeLists[0].split("-")
    val year = dateList[0].toInt()
    val month = dateList[1].toInt()
    val day = dateList[2].toInt()
    val timeList = dateTimeLists[1].split(":")
    val datetime = "${MONTHS[month - 1]} $day, $year at ${timeList[0]}:${timeList[1]}"
    val dayOfWeek = LocalDate.of(year, month, day).dayOfWeek.name

    val postponeColor = if (match.state == "postponed") {
        Color(POSTPONE_R, POSTPONE_G, POSTPONE_B)
    } else { Color.Unspecified }
    Row() {
        Column(modifier = Modifier.weight(EIGHT)) {
            Text(
                text = match.competitionStage.competition.name,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(start = 15.dp, top = 10.dp)
            )
            Row() {
                Text(
                    text = match.venue.name,
                    modifier = Modifier.padding(start = 15.dp, bottom = 15.dp),
                    fontSize = 15.sp
                )
                Text(
                    text = datetime,
                    modifier = Modifier.padding(bottom = 15.dp),
                    fontSize = 15.sp,
                    color = postponeColor
                )
            }
        }
        Column(modifier = Modifier.weight(2f)) {
            if (match.state == "postponed") {
                Button(
                    onClick = {},
                    shape = RectangleShape,
                    colors = ButtonDefaults.buttonColors(containerColor = postponeColor),
                    modifier = Modifier.size(width = 100.dp, height = 30.dp)
                ) { Text(text = "POSTPONED", color = Color.White, fontSize = 10.sp) }
            }
        }
    }
    Row() {
        Column(modifier = Modifier.weight(EIGHT)) {
            Text(text = match.homeTeam.name, fontWeight = FontWeight.Bold, modifier = Modifier.padding(15.dp))
            Text(text = match.awayTeam.name, fontWeight = FontWeight.Bold, modifier = Modifier.padding(15.dp))
        }
        Column(
            modifier = Modifier.weight(2f),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = "$day", fontSize = 35.sp, modifier = Modifier.padding(top = 20.dp, start = 10.dp))
            Text(text = dayOfWeek.take(THREE), modifier = Modifier.padding(start = 10.dp))
        }
    }
}

val MONTHS = listOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
const val THREE = 3
const val EIGHT = 8f
const val POSTPONE_R = 230
const val POSTPONE_G = 80
const val POSTPONE_B = 30
