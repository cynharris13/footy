package com.example.footy.view

import com.example.footy.model.entity.Match

/**
 * Football state.
 *
 * @property matches
 * @property isLoading
 * @property error
 * @constructor Create empty Football state
 */
data class FootballState(
    val matches: List<Match> = emptyList(),
    val isLoading: Boolean = false,
    val error: String = ""
)
