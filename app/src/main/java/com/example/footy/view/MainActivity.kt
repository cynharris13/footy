package com.example.footy.view

import androidx.fragment.app.FragmentActivity
import com.example.footy.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : FragmentActivity(R.layout.activity_main)
