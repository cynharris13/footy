package com.example.footy

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Footy application.
 *
 * @constructor Create empty Footy application
 */
@HiltAndroidApp
class FootyApplication : Application()
