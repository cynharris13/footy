package com.example.footy.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.footy.model.FootballRepo
import com.example.footy.view.FootballState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Football view model.
 *
 * @property repo
 * @constructor Create empty Football view model
 */
@HiltViewModel
class FootballViewModel @Inject constructor(private val repo: FootballRepo) : ViewModel() {
    private val _matches = MutableStateFlow(FootballState())
    val matchListState: StateFlow<FootballState> get() = _matches

    /**
     * Get matches.
     *
     */
    fun getMatches() = viewModelScope.launch {
        _matches.update { it.copy(isLoading = true) }
        val matches = repo.getMatches()
        _matches.update { it.copy(isLoading = false, matches = matches) }
    }
}
