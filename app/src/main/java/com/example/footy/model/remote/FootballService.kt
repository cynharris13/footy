package com.example.footy.model.remote

import com.example.footy.model.dto.MatchDTO
import retrofit2.Response
import retrofit2.http.GET

/**
 * Football service which retrieves match information.
 *
 * @constructor Create empty Football service
 */
interface FootballService {
    @GET("fixtures.json")
    suspend fun getMatches(): Response<List<MatchDTO>>
}
