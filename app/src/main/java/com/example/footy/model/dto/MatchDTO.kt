package com.example.footy.model.dto

@kotlinx.serialization.Serializable
data class MatchDTO(
    val awayTeam: AwayTeamDTO,
    val competitionStage: CompetitionStageDTO,
    val date: String = "",
    val homeTeam: HomeTeamDTO,
    val id: Int,
    val state: String? = null,
    val type: String = "",
    val venue: VenueDTO
)
