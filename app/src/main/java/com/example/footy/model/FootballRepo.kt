package com.example.footy.model

import com.example.footy.model.dto.MatchDTO
import com.example.footy.model.entity.Match
import com.example.footy.model.mapper.MatchMapper
import com.example.footy.model.remote.FootballService
import retrofit2.Response
import javax.inject.Inject

/**
 * Football repo.
 *
 * @property footballService
 * @constructor Create empty Football repo
 */
class FootballRepo @Inject constructor(private val footballService: FootballService) {
    val mapper = MatchMapper()

    /**
     * Get matches.
     *
     * @return
     */
    suspend fun getMatches(): List<Match> {
        val response: Response<List<MatchDTO>> = footballService.getMatches()
        return if (response.isSuccessful) {
            val matches = response.body() ?: emptyList()
            matches.map { mapper(it) }
        } else {
            emptyList()
        }
    }
}
