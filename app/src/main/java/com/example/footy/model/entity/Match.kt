package com.example.footy.model.entity

@kotlinx.serialization.Serializable
data class Match(
    val awayTeam: AwayTeam,
    val competitionStage: CompetitionStage,
    val date: String = "",
    val homeTeam: HomeTeam,
    val id: Int,
    val state: String = "",
    val type: String = "",
    val venue: Venue
)
