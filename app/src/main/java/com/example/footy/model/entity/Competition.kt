package com.example.footy.model.entity

@kotlinx.serialization.Serializable
data class Competition(
    val id: Int,
    val name: String = ""
)
