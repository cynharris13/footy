package com.example.footy.model.dto

@kotlinx.serialization.Serializable
data class AwayTeamDTO(
    val abbr: String = "",
    val alias: String = "",
    val id: Int,
    val name: String = "",
    val shortName: String = ""
)
