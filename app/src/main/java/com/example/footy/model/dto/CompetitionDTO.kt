package com.example.footy.model.dto

@kotlinx.serialization.Serializable
data class CompetitionDTO(
    val id: Int = 0,
    val name: String = ""
)
