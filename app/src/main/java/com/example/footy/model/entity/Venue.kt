package com.example.footy.model.entity

@kotlinx.serialization.Serializable
data class Venue(
    val id: Int,
    val name: String = ""
)
