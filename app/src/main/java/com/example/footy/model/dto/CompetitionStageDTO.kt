package com.example.footy.model.dto

@kotlinx.serialization.Serializable
data class CompetitionStageDTO(
    val competition: CompetitionDTO,
    val leg: String = "",
    val stage: String = ""
)
