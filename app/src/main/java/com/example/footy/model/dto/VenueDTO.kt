package com.example.footy.model.dto

@kotlinx.serialization.Serializable
data class VenueDTO(
    val id: Int,
    val name: String = ""
)
