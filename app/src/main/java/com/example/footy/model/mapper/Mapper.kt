package com.example.footy.model.mapper

/**
 * Mapper is a generic interface for converting [DTO] to [ENTITY].
 *
 * @param DTO
 * @param ENTITY
 * @constructor Create empty Mapper
 */
interface Mapper<in DTO, out ENTITY> {
    /**
     * Invoke.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
