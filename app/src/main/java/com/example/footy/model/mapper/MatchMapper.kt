package com.example.footy.model.mapper

import com.example.footy.model.dto.MatchDTO
import com.example.footy.model.entity.AwayTeam
import com.example.footy.model.entity.Competition
import com.example.footy.model.entity.CompetitionStage
import com.example.footy.model.entity.HomeTeam
import com.example.footy.model.entity.Match
import com.example.footy.model.entity.Venue

/**
 * Match mapper.
 *
 * @constructor Create empty Match mapper
 */
class MatchMapper : Mapper<MatchDTO, Match> {
    override fun invoke(dto: MatchDTO): Match = with(dto) {
        return Match(
            date = date,
            id = id,
            state = state ?: "",
            type = type,
            awayTeam = AwayTeam(
                abbr = awayTeam.abbr,
                alias = awayTeam.alias,
                id = awayTeam.id,
                name = awayTeam.name,
                shortName = awayTeam.shortName
            ),
            competitionStage = CompetitionStage(
                leg = competitionStage.leg,
                stage = competitionStage.stage,
                competition = Competition(
                    id = competitionStage.competition.id,
                    name = competitionStage.competition.name
                )
            ),
            homeTeam = HomeTeam(
                abbr = homeTeam.abbr,
                alias = homeTeam.alias,
                id = homeTeam.id,
                name = homeTeam.name,
                shortName = homeTeam.shortName
            ),
            venue = Venue(id = venue.id, name = venue.name)
        )
    }
}
