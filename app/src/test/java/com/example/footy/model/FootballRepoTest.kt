package com.example.footy.model

import com.example.footy.model.dto.AwayTeamDTO
import com.example.footy.model.dto.CompetitionDTO
import com.example.footy.model.dto.CompetitionStageDTO
import com.example.footy.model.dto.HomeTeamDTO
import com.example.footy.model.dto.MatchDTO
import com.example.footy.model.dto.VenueDTO
import com.example.footy.model.mapper.MatchMapper
import com.example.footy.model.remote.FootballService
import com.example.footy.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class FootballRepoTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    val service = mockk<FootballService>()
    val repo = FootballRepo(service)
    val mapper = MatchMapper()

    @Test
    @DisplayName("Tests fetching the match list data")
    fun testGetMatches() = runTest(extension.dispatcher) {
        // given
        val dtos = listOf(
            MatchDTO(
                awayTeam = AwayTeamDTO(id = 1),
                competitionStage = CompetitionStageDTO(CompetitionDTO()),
                homeTeam = HomeTeamDTO(id = 2),
                id = 3,
                venue = VenueDTO(id = 4)
            )
        )
        coEvery { service.getMatches().isSuccessful } coAnswers { true }
        coEvery { service.getMatches().body() } coAnswers { dtos }

        // when
        val actual = repo.getMatches()

        val expected = dtos.map { mapper(it) }
        // then
        Assertions.assertEquals(expected, actual)
    }
}
