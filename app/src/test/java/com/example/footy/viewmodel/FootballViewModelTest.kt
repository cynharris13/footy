package com.example.footy.viewmodel

import com.example.footy.model.FootballRepo
import com.example.footy.model.entity.AwayTeam
import com.example.footy.model.entity.Competition
import com.example.footy.model.entity.CompetitionStage
import com.example.footy.model.entity.HomeTeam
import com.example.footy.model.entity.Match
import com.example.footy.model.entity.Venue
import com.example.footy.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class FootballViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<FootballRepo>()
    val viewModel = FootballViewModel(repo)

    @Test
    @DisplayName("Tests the viewmodel retrieving match data from the repo")
    fun testGetMatches() = runTest(extension.dispatcher) {
        // given
        val expected = listOf(
            Match(
                awayTeam = AwayTeam(id = 1),
                competitionStage = CompetitionStage(Competition(id = 5)),
                homeTeam = HomeTeam(id = 2),
                id = 3,
                venue = Venue(id = 4)
            )
        )
        coEvery { repo.getMatches() } coAnswers { expected }

        // when
        viewModel.getMatches()

        // then
        Assertions.assertFalse(viewModel.matchListState.value.isLoading)
        Assertions.assertEquals(expected, viewModel.matchListState.value.matches)
    }
}
